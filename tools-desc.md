# Tools

## Babel

Babel est un transcompilateur JavaScript gratuit et open source qui est principalement utilisé pour convertir le code ECMAScript 2015+ en une version rétrocompatible de JavaScript pouvant être exécutée par des moteurs JavaScript plus anciens.
